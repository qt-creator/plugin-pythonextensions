# We cannot use qtcreatordata.pri for this, because that cannot be made "on request only"
# so we use a "no_default_install" install target instead.

TEMPLATE = aux

include(../plugins/pythonextensions/qtcreator.pri)
include($$IDE_SOURCE_TREE/qtcreator.pri)

inst_examples.files = \
    projects \
    requirerequests \
    smallmenu \
    transform

inst_examples.path = $$IDE_PLUGIN_PATH/python
inst_examples.CONFIG += no_default_install directory
INSTALLS += inst_examples

inst_examplefiles.files = \
    examples_common.py \
    macroexpander.py

inst_examplefiles.path = $$IDE_PLUGIN_PATH/python
inst_examplefiles.CONFIG += no_default_install
INSTALLS += inst_examplefiles

copyexamples.depends += install_inst_examples install_inst_examplefiles
QMAKE_EXTRA_TARGETS += copyexamples
