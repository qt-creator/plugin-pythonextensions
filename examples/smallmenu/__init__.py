#############################################################################
##
## Copyright (C) 2018 The Qt Company Ltd.
## Contact: http://www.qt.io/licensing/
##
## This file is part of the Python Extensions Plugin for Qt Creator.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of The Qt Company Ltd nor the names of its
##     contributors may be used to endorse or promote products derived
##     from this software without specific prior written permission.
##
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
## $QT_END_LICENSE$
##
#############################################################################

# This example illustrates the use of action
# containers to create new menus

from .actions import hello, goodbye
import examples_common

from QtCreator import Core
from PySide2.QtCore import QObject, SIGNAL
from PySide2.QtWidgets import QAction

# Create submenu and add it to examples menu
menuId = "PythonExtensions.Examples.SmallMenu"
menu = Core.ActionManager.createMenu(menuId)
menu.menu().setTitle("Small Menu")
examplesMenu = examples_common.ensureExamplesMenu()
examplesMenu.addMenu(menu)

# Add actions to our new menu
helloAction = QAction("Say Hello")
command = Core.ActionManager.registerAction(helloAction, 'PythonExtensions.Examples.SayHello')
QObject.connect(helloAction, SIGNAL('triggered()'), hello)
menu.addAction(command)

goodbyeAction = QAction("Say Goodbye")
command = Core.ActionManager.registerAction(goodbyeAction, 'PythonExtensions.Examples.SayGoodbye')
QObject.connect(goodbyeAction, SIGNAL('triggered()'), goodbye)
menu.addAction(command)
