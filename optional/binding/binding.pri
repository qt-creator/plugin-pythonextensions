DEFINES += PYTHONEXTENSIONS_LIBRARY

# PythonExtensions files

SOURCES += \
    binding.cpp

HEADERS += \
    $$PWD/binding.h

INCLUDEPATH *= $$PWD

QTC_PLUGIN_DIRS += $$PWD/../../plugins
include(../../plugins/pythonextensions/qtcreator.pri)
include($$IDE_SOURCE_TREE/src/qtcreatorplugin.pri)
