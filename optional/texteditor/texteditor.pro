
# Declare dependencies and name

# This has to be PythonBinding{PluginName}
QTC_PLUGIN_NAME = PythonBindingTextEditor
QTC_LIB_DEPENDS += \
    extensionsystem \
    utils

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    texteditor \
    pythonextensions

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

include(../binding/binding.pri)

INCLUDEPATH *= $$IDE_SOURCE_TREE/src/plugins/texteditor

# Shiboken binding generation setup

WRAPPED_HEADER = $$PWD/wrappedclasses.h
WRAPPER_DIR = $$OUT_PWD/QtCreatorBindingTextEditor
TYPESYSTEM_FILE = typesystem_texteditor.xml
TYPESYSTEM_NAME = qtcreatorbindingtexteditor
SHIBOKEN_QT = core gui widgets

## These headers are needed so the generated wrappers are added to the
## build. Right now they are empty files, however there might be a more elegant
## option.
WRAPPED_CLASSES = \
  bindingheaders/texteditor.h \
# Sentinel line

include(../../shiboken.pri)
