DEFINES += PYTHONEXTENSIONS_LIBRARY

# PythonExtensions files

SOURCES += \
        pythonextensionsplugin.cpp \
        pyutil.cpp

HEADERS += \
        pythonextensionsplugin.h \
        pythonextensions_global.h \
        pyutil.h

INCLUDEPATH += $$PWD

# Qt Creator linking

# Shared QtCreator sources and build destination
# (these are shared by this and the optional bindings)
include(qtcreator.pri)

include($$IDE_SOURCE_TREE/src/qtcreatorplugin.pri)

# Qt Creator Core bindings

WRAPPED_HEADER = $$PWD/wrappedclasses.h
WRAPPER_DIR = $$OUT_PWD/QtCreator
TYPESYSTEM_FILE = typesystem_qtcreator.xml
TYPESYSTEM_NAME = qtcreator
SHIBOKEN_QT = core gui widgets

## These headers are needed so the generated wrappers are added to the
## build. Right now they are empty files, however there might be a more elegant
## option.
WRAPPED_CLASSES = \
  bindingheaders/pythonextensions.h \
  bindingheaders/pythonextensions_internal.h \
  bindingheaders/pythonextensions_internal_pythonextensionsplugin.h \
  bindingheaders/pythonextensions_extension.h \
  bindingheaders/core.h \
  bindingheaders/core_actioncontainer.h \
  bindingheaders/core_actionmanager.h \
  bindingheaders/core_command.h \
  bindingheaders/core_constants.h \
  bindingheaders/core_icontext.h \
  bindingheaders/core_icore.h \
  bindingheaders/core_id.h \
  bindingheaders/core_context.h \
  bindingheaders/core_editormanager.h \
  bindingheaders/core_ieditor.h \
  bindingheaders/core_idocument.h \
  bindingheaders/core_documentmanager.h \
  bindingheaders/core_documentmodel.h \
  bindingheaders/core_fileutils.h \
  bindingheaders/core_messagemanager.h \
  bindingheaders/utils.h \
  bindingheaders/utils_macroexpander.h \
  bindingheaders/utils_filename.h \
  bindingheaders/extensionsystem.h \
  bindingheaders/extensionsystem_iplugin.h
# Sentinel line

include(../../shiboken.pri)
