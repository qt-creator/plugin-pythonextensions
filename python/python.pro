TEMPLATE = aux

include($$IDE_SOURCE_TREE/qtcreator.pri)

STATIC_BASE = $$PWD
STATIC_OUTPUT_BASE = $$IDE_PLUGIN_PATH/python
STATIC_INSTALL_BASE = $$INSTALL_PLUGIN_PATH/python

DATA_DIRS = extensionmanager

for(data_dir, DATA_DIRS) {
    files = $$files($$PWD/$$data_dir/*, true)
    for(file, files): STATIC_FILES += $$file
}

include($$IDE_SOURCE_TREE/qtcreatordata.pri)
