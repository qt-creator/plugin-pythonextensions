# Usage: Define variables (details below) and include this pri file afterwards.
#
# WRAPPED_HEADER
# WRAPPER_DIR
# TYPESYSTEM_FILE
# TYPESYSTEM_NAME
# SHIBOKEN_QT
# WRAPPED_CLASSES

PYTHON = python

include(plugins/pythonextensions/pyside2.pri)

# Qt include paths
for (qt, SHIBOKEN_QT) {
    SHIBOKEN_INCLUDEPATHS *= $$eval(QT.$${qt}.includes)
    SHIBOKEN_FRAMEWORKPATHS *= $$eval(QT.$${qt}.frameworks)
}

# Qt Creator include paths
SHIBOKEN_INCLUDEPATHS *= $$INCLUDEPATH
for (path, SHIBOKEN_INCLUDEPATHS): SHIBOKEN_CXXFLAGS *= -I$$path
for (path, SHIBOKEN_FRAMEWORKPATHS): SHIBOKEN_CXXFLAGS *= -F$$path

SHIBOKEN_OPTIONS = --generator-set=shiboken --enable-parent-ctor-heuristic \
    --enable-pyside-extensions --enable-return-value-heuristic --use-isnull-as-nb_nonzero \
    $$SHIBOKEN_CXXFLAGS -T$$PYSIDE2/typesystems --output-directory=$$OUT_PWD

win32: SHIBOKEN_OPTIONS += --avoid-protected-hack

## Prepare the shiboken tool
QT_TOOL.shiboken.binary = $$system_path($$PYSIDE2/shiboken2)
qtPrepareTool(SHIBOKEN, shiboken)

## Shiboken run that adds the module wrapper to GENERATED_SOURCES
shiboken.output = $$WRAPPER_DIR/$${TYPESYSTEM_NAME}_module_wrapper.cpp
shiboken.commands = $$SHIBOKEN $$SHIBOKEN_OPTIONS $$WRAPPED_HEADER ${QMAKE_FILE_IN}
shiboken.input = TYPESYSTEM_FILE
shiboken.dependency_type = TYPE_C
shiboken.variable_out = GENERATED_SOURCES

module_wrapper_dummy_command.output = $$WRAPPER_DIR/${QMAKE_FILE_BASE}_wrapper.cpp
module_wrapper_dummy_command.commands = echo ${QMAKE_FILE_IN}
module_wrapper_dummy_command.depends = $$WRAPPER_DIR/$${TYPESYSTEM_NAME}_module_wrapper.cpp
module_wrapper_dummy_command.input = WRAPPED_CLASSES
module_wrapper_dummy_command.dependency_type = TYPE_C
module_wrapper_dummy_command.variable_out = GENERATED_SOURCES

QMAKE_EXTRA_COMPILERS += shiboken module_wrapper_dummy_command

# TODO: Fix some more of these hardcoded include paths
INCLUDEPATH += $$WRAPPER_DIR \
    $$PWD/plugins/pythonextensions \
    $$shadowed($$PWD)/plugins/pythonextensions/QtCreator \
    "$$IDE_SOURCE_TREE/src/plugins/coreplugin" \
    "$$IDE_SOURCE_TREE/src/plugins/coreplugin/actionmanager" \
    "$$IDE_SOURCE_TREE/src/plugins/coreplugin/editormanager" \
    "$$IDE_SOURCE_TREE/src/libs/extensionsystem" \
    "$$IDE_SOURCE_TREE/src/libs/utils"

for(i, PYSIDE2_INCLUDE) {
    for (qt, SHIBOKEN_QT): INCLUDEPATH += $$i/$$eval(QT.$${qt}.name)
}
